#!/bin/bash

pu_score()
{
        # This is a function that calculates a PU (probability unfolded) score for a given RNA sequence
        # The size of the RNA sequence window is restricted to 21 nt as this has been experimentally 
        # confirmed to be the size of the region behind RNA polymerase II that is free to fold before
        # being bound by regulatory proteins. The calculation of the score assumes that RNA folding 
        # energies are Boltzmann distributed.

        input="$1"
        c_string="$2"
        prefix="$3"

        # Identify all fifteen sequence windows that may be associated with the variant
        # Construct a FASTA for RNAfold
        for (( i=1; i<=15; i++ )); do
                j=$(bc -l <<< "$i+20")
                tmpseq=$(echo "$input" | cut -c"$i"-"$j")
                tmp_c=$(echo "$c_string" | cut -c"$i"-"$j")
                echo -e ">$i\t$tmpseq\t$tmp_c" | tr '\t' '\n' >> "$tmp/$prefix.fa"
        done

        # Call RNAfold twice: (1) without folding constraints, (2) with specified folding constraints to 
        # measure ensemble folding energy
        RNAfold -p -d2 --noLP < "$tmp/$prefix.fa" | awk 'NR%6==4' | cut -d '[' -f2 | sed 's/^ //' | sed 's/]$//' > "$tmp/$prefix.u_free"
        RNAfold -p -d2 --noLP -C < "$tmp/$prefix.fa" | awk 'NR%6==4' | cut -d '[' -f2 | sed 's/^ //' | sed 's/]$//' > "$tmp/$prefix.u_constraint"

        # Compute the PU based on RNAfold ensemble folding energies
        score=$(paste "$tmp/$prefix.u_free" "$tmp/$prefix.u_constraint" | awk '{print exp(($1-$2)*(4184/(8.31*293)))}' | sort -g | head -n 1)
        echo "$score"

        rm "$tmp/$prefix.fa"
        rm "$tmp/$prefix.u_free"
        rm "$tmp/$prefix.u_constraint"

}
export tmp
export -f pu_score

annotate_rnafold()
{
        # This is a function that pre-processes sequences associated with each variant in the training file
        # This function also uses the outputs of function pu_score to quantify the impact of a variant on 
        # pre-mRNA secondary structure formation

        prefix=$(basename "$1")

        # Construct RNA folding constraint string
        # Assumes that there exists a regulatory motif of size 7 bp centered about the variant
        c_flank=$(perl -E 'say "." x 14')
        c_motif=$(perl -E 'say "x" x 7')
        c_string="$c_flank""$c_motif""$c_flank"

        while read line; do
                variant_id=$(echo "$line" | cut -f1)
                chr=$(echo "$line" | cut -f2)
                pos=$(echo "$line" | cut -f3)
                ref=$(echo "$line" | cut -f4)
                alt=$(echo "$line" | cut -f5)
                strand=$(echo "$line" | cut -f9)

                lower_bound=$(bc -l <<< "$pos-17")
                upper_bound=$(bc -l <<< "$pos+17")

                # Extract reference sequence specified by lower and upper bounds     
                refseq=$(tail -n +2 "$hg19/$chr.fa" | cut -c"$lower_bound"-"$upper_bound" | tr "[a-z]" "[A-Z]")

                # Take reverse complement if strand orientation is -
                if [ "$strand" == "-" ]; then
                        refseq=$(echo "$refseq" | sed 'y/ATCG/TAGC/;s/^.*$/X&X/;:x;s/\(X.\)\(.*\)\(.X\)/\3\2\1/;tx;s/X//g')
                fi

                # Get PU (probability unfolded) score for reference sequence
                prob_ref=$(pu_score "$refseq" "$c_string" "$prefix")

                len_ref=${#ref}
                len_alt=${#alt}

                # Adjust upper bound if there is a deletion
                if [ "$len_ref" -gt "$len_alt" ]; then
                        len_diff=$(bc -l <<< "$len_ref-$len_alt")
                        upper_bound=$(bc -l <<< "$pos+17+$len_diff")
                fi

                # Construct mutated sequence 
                refseq=$(tail -n +2 "$hg19/$chr.fa" | cut -c"$lower_bound"-"$upper_bound" | tr "[a-z]" "[A-Z]")
                alt_left=$(echo "$refseq" | cut -c1-17)
                idx_right=$(bc -l <<< "$len_ref+18")
                alt_right=$(echo "$refseq" | cut -c"$idx_right"-)
                altseq="$alt_left""$alt""$alt_right"

                # Crop mutated sequence if there is an insertion
                if [ "$len_ref" -lt "$len_alt" ]; then
                        altseq=$(echo "$altseq" | cut -c1-35)
                fi

                # Take reverse complement if strand orientation is -
                if [ "$strand" == "-" ]; then
                        altseq=$(echo "$altseq" | sed 'y/ATCG/TAGC/;s/^.*$/X&X/;:x;s/\(X.\)\(.*\)\(.X\)/\3\2\1/;tx;s/X//g')
                fi

                # Get PU (probability unfolded) score for alternate sequence
                prob_alt=$(pu_score "$altseq" "$c_string" "$prefix")

                echo -e "$variant_id\t$prob_ref\t$prob_alt" >> "$tmp/$prefix.rnafold"
        done < "$1"
}

export hg19
export -f annotate_rnafold

# Process input arguments: (1) input file of variants, (2) path to a tmp directory to store intermediate results
input_file="$1"
tmp="$2"

# Setup working directories
data="/data/rwang/data"
hg19="$data/hg19"
src_dir=$(dirname "$0")
prefix_out=$(basename "$input_file" | cut -d '.' -f1)

threads=$(nproc)
num_lines=$(tail -n +2 "$input_file" | wc -l)
lines_per_file=$(awk 'BEGIN {printf("%.0f",('"$num_lines"'+'"$threads"'-1)/'"$threads"')}')
tail -n +2 "$input_file" | split --lines="$lines_per_file" - "$tmp/vs_input."

# Split input file of variants for parallel processing
find "$tmp" -name "vs_input.*" -print0 | parallel -0 -n 1 -P "$threads" -I '{}' annotate_rnafold '{}'

# Remove post-script intermediate files in source directory (produced by running RNAfold)
rm $src_dir/*.ps

# Combine annotations into one file
find "$tmp" -name "vs_input.*.rnafold" | while read rnafold; do
        cat "$rnafold" >> "$tmp/vs_input.rnafold.unsorted"
done

# Set up header for output file
echo -e "variant_id\tPU_ref\tPU_alt" > "$tmp/$prefix_out.rnafold"
sort -k1,1 -k2,2n "$tmp/vs_input.rnafold.unsorted" >> "$tmp/$prefix_out.rnafold"

rm $tmp/vs_input.*