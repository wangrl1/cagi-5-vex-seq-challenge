#!/bin/bash

svm_bp()
{
        # This is a function that calculates the change in branch point sequence strength
        # due to a mutation (SNV/small indel) using SVM-BP

        prefix=$(basename "$1")

        while read line; do
                chr=$(echo "$line" | cut -f2)
                pos=$(echo "$line" | cut -f3)
                ref=$(echo "$line" | cut -f4)
                alt=$(echo "$line" | cut -f5)
                exon_start=$(echo "$line" | cut -f6)
                exon_stop=$(echo "$line" | cut -f7)
                strand=$(echo "$line" | cut -f9)

                # Extract intronic sequence (60 bp size) upstream of the 3'-splice site (range 
                # should encapsulate branch point sequences) 

                if [ "$strand" == "+" ]; then
                        lower_bound=$(bc -l <<< "$exon_start-60")
                        upper_bound=$(bc -l <<< "$exon_start-1")
                else
                        lower_bound=$(bc -l <<< "$exon_stop+1")
                        upper_bound=$(bc -l <<< "$exon_stop+60")
                fi

                # If the variant position is not within the relevant bounds for the test exon
                # report that the variant does not impact the branch point sequence associated
                # with the test exon.

                if [ "$pos" -ge "$lower_bound" ] && [ "$pos" -le "$upper_bound" ]; then
                        # Extract the reference sequence
                        refseq=$(tail -n +2 "$hg19/$chr.fa" | cut -c"$lower_bound"-"$upper_bound" | tr "[a-z]" "[A-Z]")

                        # Take the reverse complement of the sequence if the orientation is -
                        if [ "$strand" == "-" ]; then
                                refseq=$(echo "$refseq" | sed 'y/ATCG/TAGC/;s/^.*$/X&X/;:x;s/\(X.\)\(.*\)\(.X\)/\3\2\1/;tx;s/X//g')
                        fi

                        # Construct FASTA containing reference sequence and pass into SVM-BP
                        echo -e ">reference\n$refseq" > "$tmp/$prefix.ref.fa"
                        python "/data/rwang/data/SVMBP/svm_bpfinder.py" -i "$tmp/$prefix.ref.fa" -s "Hsap" -p "$prefix" -o "$tmp" > "$tmp/$prefix.ref.svmbp"

                        # If SVM-BP identifies potential branch point sequences, output the best one 
                        if [ -s "$tmp/$prefix.ref.svmbp" ]; then
                                svm_ref=$(perl "/data/rwang/data/SVMBP/calculate_best_BP_per_intron.pl" < "$tmp/$prefix.ref.svmbp" | cut -f10)
                                if [ -z "$svm_ref" ]; then
                                        svm_ref="0"
                                fi
                        else
                                svm_ref="0"
                        fi

                        len_ref=${#ref}
                        len_alt=${#alt}

                        # Deletion
                        if [ "$len_ref" -gt "$len_alt" ]; then
                                len_diff=$(bc -l <<< "$len_ref-$len_alt")
                                if [ "$strand" == "+" ]; then
                                        lower_bound=$(bc -l <<< "$exon_start-60-$len_diff")
                                else
                                        upper_bound=$(bc -l <<< "$exon_stop+60+$len_diff")
                                fi
                        fi

                        # Construct the mutated sequence 
                        refseq=$(tail -n +2 "$hg19/$chr.fa" | cut -c"$lower_bound"-"$upper_bound" | tr "[a-z]" "[A-Z]")
                        idx_left=$(bc -l <<< "$pos-$lower_bound")
                        idx_right=$(bc -l <<< "$idx_left+$len_ref+1")
                        if [ ! "$idx_left" -eq "0" ]; then
                                alt_left=$(echo "$refseq" | cut -c1-"$idx_left")
                        else
                                alt_left=""
                        fi
                        alt_right=$(echo "$refseq" | cut -c"$idx_right"-)
                        altseq="$alt_left""$alt""$alt_right"

                        # Insertion
                        if [ "$len_ref" -lt "$len_alt" ]; then
                                if [ "$strand" == "-" ]; then
                                        altseq=$(echo "$altseq" | cut -c1-60)
                                else
                                        altseq=$(echo "$altseq" | rev | cut -c1-60 | rev)
                                fi
                        fi

                        # Take the reverse complement of a sequence if the orientation is -
                        if [ "$strand" == "-" ]; then
                                altseq=$(echo "$altseq" | sed 'y/ATCG/TAGC/;s/^.*$/X&X/;:x;s/\(X.\)\(.*\)\(.X\)/\3\2\1/;tx;s/X//g')
                        fi

                        # Construct FASTA containing mutated sequence and pass into SVM-BP
                        echo -e ">variant\n$altseq" > "$tmp/$prefix.alt.fa"
                        python "/data/rwang/data/SVMBP/svm_bpfinder.py" -i "$tmp/$prefix.alt.fa" -s "Hsap" -p "$prefix" -o "$tmp" > "$tmp/$prefix.alt.svmbp"

                        # If SVM-BP identifies potential branch point sequences, output the best one 
                        if [ -s "$tmp/$prefix.alt.svmbp" ]; then
                                svm_alt=$(perl "/data/rwang/data/SVMBP/calculate_best_BP_per_intron.pl" < "$tmp/$prefix.alt.svmbp" | cut -f10)
                                if [ -z "$svm_alt" ]; then
                                        svm_alt="0"
                                fi
                        else
                                svm_alt="0"
                        fi

                        # Compute the difference in branch point sequence strengths between the mutated sequence and reference sequences
                        svm_score=$(awk -v "svm_alt=$svm_alt" -v "svm_ref=$svm_ref" 'BEGIN{print svm_alt-svm_ref}')

                        rm "$tmp/$prefix.ref.svmbp"
                        rm "$tmp/$prefix.alt.svmbp"
                        rm "$tmp/$prefix.ref.fa"
                        rm "$tmp/$prefix.alt.fa"

                else
                        svm_score="0"
                fi

                echo -e "$chr\t$pos\t$ref\t$alt\t$svm_score" >> "$tmp/$prefix.svmbp"

        done < "$1"
}

export tmp
export hg19
export -f svm_bp

# Process input arguments: (1) input file of variants, (2) path to a tmp directory to store intermediate results
input_file="$1"
tmp="$2"

# Set up working directories
data="/data/rwang/data"
hg19="$data/hg19"
src_dir=$(dirname "$0")
prefix_out=$(basename "$input_file" | cut -d '.' -f1)

threads=$(nproc)
num_lines=$(tail -n +2 "$input_file" | wc -l)
lines_per_file=$(awk 'BEGIN {printf("%.0f",('"$num_lines"'+'"$threads"'-1)/'"$threads"')}')
tail -n +2 "$input_file" | split --lines="$lines_per_file" - "$tmp/vs_input."

# Split input file of variants for parallel processing
find "$tmp" -name "vs_input.*" -print0 | parallel -0 -n 1 -P "$threads" -I '{}' svm_bp '{}'

# Combine annotations into one file
find "$tmp" -name "vs_input.*.svmbp" | while read file_svmbp; do
        cat "$file_svmbp" >> "$tmp/vs_input.svmbp.unsorted"
done

# Set up header for output file
echo -e "chromosome\thg19_variant_position\treference\tvariant\tSVM_BP" > "$tmp/$prefix_out.svmbp"
sort -k1,1 -k2,2n "$tmp/vs_input.svmbp.unsorted" >> "$tmp/$prefix_out.svmbp"

rm $tmp/vs_input.*