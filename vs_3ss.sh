#!/bin/bash

score_3ss()
{
        # This is a function that calculates the change in 3'-splice site strength
        # due to a mutation (SNV/small indel) using MaxEntScan::3ss

        prefix=$(basename "$1")

        while read line; do
                chr=$(echo "$line" | cut -f2)
                pos=$(echo "$line" | cut -f3)
                ref=$(echo "$line" | cut -f4)
                alt=$(echo "$line" | cut -f5)
                exon_start=$(echo "$line" | cut -f6)
                exon_stop=$(echo "$line" | cut -f7)
                strand=$(echo "$line" | cut -f9)

                # Extract relevant bounds for running MaxEntScan:3ss
                # [20 bases in intron][3 bases in exon]

                if [ "$strand" == "+" ]; then
                        lower_bound=$(bc -l <<< "$exon_start-20")
                        upper_bound=$(bc -l <<< "$exon_start+2")
                else
                        lower_bound=$(bc -l <<< "$exon_stop-2")
                        upper_bound=$(bc -l <<< "$exon_stop+20")
                fi

                # If the variant position is not within the relevant bounds for the test exon
                # report that the variant does not impact the 3'-splice site of the test exon

                if [ "$pos" -ge "$lower_bound" ] && [ "$pos" -le "$upper_bound" ]; then
                        # Extract the reference sequence
                        refseq=$(tail -n +2 "$hg19/$chr.fa" | cut -c"$lower_bound"-"$upper_bound" | tr "[a-z]" "[A-Z]")

                        # Take the reverse complement of the sequence if the orientation is -
                        if [ "$strand" == "-" ]; then
                                refseq=$(echo "$refseq" | sed 'y/ATCG/TAGC/;s/^.*$/X&X/;:x;s/\(X.\)\(.*\)\(.X\)/\3\2\1/;tx;s/X//g')
                        fi

                        # Calculate the 3'-splice site strength of the reference sequence
                        mes_ref=$(echo "$refseq" | perl "/data/rwang/data/MaxEntScan/score3.pl" - | cut -f2)

                        len_ref=${#ref}
                        len_alt=${#alt}

                        # SNP/Insertion
                        if [ "$len_ref" -le "$len_alt" ]; then
                                # Construct the alternate sequence
                                refseq=$(tail -n +2 "$hg19/$chr.fa" | cut -c"$lower_bound"-"$upper_bound" | tr "[a-z]" "[A-Z]")
                                idx_left=$(bc -l <<< "$pos-$lower_bound")
                                idx_right=$(bc -l <<< "$idx_left+$len_ref+1")
                                if [ ! "$idx_left" -eq "0" ]; then
                                        alt_left=$(echo "$refseq" | cut -c1-"$idx_left")
                                else
                                        alt_left=""
                                fi
                                alt_right=$(echo "$refseq" | cut -c"$idx_right"-)
                                altseq="$alt_left""$alt""$alt_right"

                                # Trim mutated sequence based on adjusted bounds
                                if [ "$len_ref" -lt "$len_alt" ]; then
                                        if [ "$strand" == "+" ]; then
                                                if [ "$pos" -lt "$exon_start" ]; then
                                                        start_idx=$(bc -l <<< "$len_alt-$len_ref+1")
                                                        altseq=$(echo "$altseq" | cut -c"$start_idx"-)
                                                else
                                                        altseq=$(echo "$altseq" | cut -c1-23)
                                                fi
                                        else
                                                if [ "$pos" -le "$exon_stop" ]; then
                                                        start_idx=$(bc -l <<< "$len_alt-$len_ref+1")
                                                        altseq=$(echo "$altseq" | cut -c"$start_idx"-)
                                                else
                                                        altseq=$(echo "$altseq" | cut -c1-23)
                                                fi
                                        fi
                                fi

                                # Take reverse complement of sequence if strand orientation is -
                                if [ "$strand" == "-" ]; then
                                        altseq=$(echo "$altseq" | sed 'y/ATCG/TAGC/;s/^.*$/X&X/;:x;s/\(X.\)\(.*\)\(.X\)/\3\2\1/;tx;s/X//g')
                                fi
                        
                        # Deletions
                        else
                                if [ "$strand" == "+" ]; then
                                        if [ "$pos" -lt "$exon_start" ]; then
                                                lower_bound=$(bc -l <<< "$exon_start-$len_ref+$len_alt-20")
                                        else
                                                upper_bound=$(bc -l <<< "$exon_start+$len_ref-$len_alt+2")
                                        fi
                                else
                                        if [ "$pos" -le "$exon_stop" ]; then
                                                lower_bound=$(bc -l <<< "$exon_stop-$len_ref+$len_alt-2")
                                        else
                                                upper_bound=$(bc -l <<< "$exon_stop+$len_ref-$len_alt+20")
                                        fi
                                fi

                                # Construct mutated sequence based on adjusted bounds
                                refseq=$(tail -n +2 "$hg19/$chr.fa" | cut -c"$lower_bound"-"$upper_bound" | tr "[a-z]" "[A-Z]")
                                idx_left=$(bc -l <<< "$pos-$lower_bound")
                                idx_right=$(bc -l <<< "$idx_left+$len_ref+1")
                                if [ ! "$idx_left" -eq "0" ]; then
                                        alt_left=$(echo "$refseq" | cut -c1-"$idx_left")
                                else
                                        alt_left=""
                                fi
                                alt_right=$(echo "$refseq" | cut -c"$idx_right"-)
                                altseq="$alt_left""$alt""$alt_right"

                                # Take reverse complement of sequence if strand orientation is -
                                if [ "$strand" == "-" ]; then
                                        altseq=$(echo "$altseq" | sed 'y/ATCG/TAGC/;s/^.*$/X&X/;:x;s/\(X.\)\(.*\)\(.X\)/\3\2\1/;tx;s/X//g')
                                fi

                        fi

                        # Calculate the 3'-splice site strength of the mutated sequence
                        mes_alt=$(echo "$altseq" | perl "/data/rwang/data/MaxEntScan/score3.pl" - | cut -f2)
                        mes_score=$(awk -v "mes_alt=$mes_alt" -v "mes_ref=$mes_ref" 'BEGIN{print mes_alt-mes_ref}')
                else
                        mes_score="0"
                fi

                echo -e "$chr\t$pos\t$ref\t$alt\t$mes_score" >> "$tmp/$prefix.3ss"

        done < "$1"
}

export tmp
export hg19
export -f score_3ss

# Process input arguments: (1) input file of variants, (2) path to a tmp directory to store intermediate results
input_file="$1"
tmp="$2"

# Set up working directories
data="/data/rwang/data"
hg19="$data/hg19"
src_dir=$(dirname "$0")
prefix_out=$(basename "$input_file" | cut -d '.' -f1)

threads=$(nproc)
num_lines=$(tail -n +2 "$input_file" | wc -l)
lines_per_file=$(awk 'BEGIN {printf("%.0f",('"$num_lines"'+'"$threads"'-1)/'"$threads"')}')
tail -n +2 "$input_file" | split --lines="$lines_per_file" - "$tmp/vs_input."

# Split input file of variants for parallel processing
find "$tmp" -name "vs_input.*" -print0 | parallel -0 -n 1 -P "$threads" -I '{}' score_3ss '{}'

# Combine annotations into one file
find "$tmp" -name "vs_input.*.3ss" | while read file_3ss; do
        cat "$file_3ss" >> "$tmp/vs_input.3ss.unsorted"
done

# Set up header for output file
echo -e "chromosome\thg19_variant_position\treference\tvariant\tMaxEntScan_3ss" > "$tmp/$prefix_out.3ss"
sort -k1,1 -k2,2n "$tmp/vs_input.3ss.unsorted" >> "$tmp/$prefix_out.3ss"

rm $tmp/vs_input.*