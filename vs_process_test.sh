#!/bin/bash

# Process input arguments: (1) test set of variants, (2) path to a tmp directory to store intermediate results
test_file="$1"
tmp="$2"

# Set up working directories
data="/data/rwang/data"
vexseq="$data/vexseq"
gencode="$data/gencode"

# Determine gene orientation of each test exon using GENCODE v19 annotations
tail -n +2 "$test_file" | cut -f2,6,7 | sort -k1,1 -k2,2n | uniq | bedtools intersect -a "$gencode/gencode.v19.exon.bed" -b - > "$tmp/vs_test_exon_coordinates.tsv"

# Setup new header for test file
echo -e "ID\tchromosome\thg19_variant_position\treference\tvariant\texon_start\texon_end\tHepG2_delta_psi\tgene_orientation" > "$tmp/vs_test_set.tsv"

# Annotate test file with test exon orientation information 
tail -n +2 "$test_file" | awk '{
        if(FNR == NR) {
                exon_id = $1_$2_$3;
                orientation[exon_id] = $4;
        }
        else {
                printf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",$1,$2,$3,$4,$5,$6,$7,$8,orientation[$2_$6_$7])
        }
}' "$tmp/vs_test_exon_coordinates.tsv" - | sort -k2,2 -k3,3n >> "$tmp/vs_test_set.tsv"

rm "$tmp/vs_test_exon_coordinates.tsv"