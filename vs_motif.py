#!/usr/bin/python

# This is a script for calculating the impact of a variant on putative
# splicing regulatory element activity

# Input Parameters:
#       1. reference sequence
#       2. mutated sequence
#       3. maximum entropy profile
#       4. window size
#       5. list of associated splicing factors
#       6. chromosome
#       7. position
#       8. orientation
#       9. variant ID
#       10. binary indication of whether variant is exonic or intronic
#       11. prefix for output file

import os
import sys
import math

# Process input parameters
refseq = sys.argv[1].strip()
altseq = sys.argv[2].strip()
profile = open(sys.argv[3])
window_size = int(sys.argv[4].strip())
factor_string = sys.argv[5].strip()
chromosome = sys.argv[6].strip()
position = sys.argv[7].strip()
orientation = sys.argv[8].strip()
variant_id = sys.argv[9].strip()
prefix = sys.argv[10].strip()

# Build dictionary mapping each k-mer to a probability
d = {}
for line in profile:
        arr = line.strip().split("\t")
        d[arr[0]] = float(arr[1])

# Process each associated splicing factor:
factor_list = factor_string.split(",")

# Extract PU scores for reference and variant sequences
q1 = "grep -P " + variant_id + "\t " + prefix + ".rnafold | cut -f2"
q2 = "grep -P " + variant_id + "\t " + prefix + ".rnafold | cut -f3"

PU_ref = float(os.popen(q1).read().strip())
PU_alt = float(os.popen(q2).read().strip())

score = 0.0
# Iterate through each factor in the list
for factor in factor_list:
        # Assume that there is a very small eCLIP binding signal associated with the sequence
        signal = -33.22
        query = "tabix /data/rwang/data/encode/HepG2/eCLIP/" + factor + ".bed.gz " + chromosome + ":" + position + "-" + position
        arr = filter(None, os.popen(query).read().strip().split("\t"))
        if len(arr) != 0:
                if arr[5] == orientation:
                        signal = float(arr[6])

        # Iterate through each k-mer window in the sequences
        for i in range(window_size):
                # Extract the reference and mutated sequence frame
                ref_frame = refseq[i:i+window_size]
                alt_frame = altseq[i:i+window_size]

                # Add a very small pseudolikelihood to the probability that the factor bindings to the given k-mer
                ref_p = d[ref_frame.replace("T","U")] + 1e-10
                alt_p = d[alt_frame.replace("T","U")] + 1e-10

                if ref_p != 1e-10 and alt_p != 1e-10:
                        # Rough estimation of equilibrium constants based on probability of binding
                        ref_K = ref_p/(1-ref_p)
                        alt_K = alt_p/(1-alt_p)

                        difference = math.log(alt_K/ref_K)/math.log(2) + math.log(PU_alt/PU_ref)/math.log(2)
                        score = score + difference

print score/len(factor_list)