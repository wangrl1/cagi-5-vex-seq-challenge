#!/usr/bin/Rscript

# Set up working libraries
R_lib <- "/h/robertwang/Rpackages"
library(withr,lib=R_lib)
library(ggplot2,lib=R_lib)
library(labeling,lib=R_lib)
library(digest,lib=R_lib)

# Parse input arguments from command line
args = commandArgs(trailingOnly=TRUE)
predictions_file <- args[1]
output_fp <- args[2]

# Read file comparing experimentally measured delta PSI values and predicted delta PSI values
predictions_df <- read.table(predictions_file, header=FALSE, sep="\t")
names(predictions_df) <- c("ID", "actual", "predicted")

# Compute Pearson and Spearman correlation between actual values and predicted values
pearson <- cor(predictions_df$actual, predictions_df$predicted, method="pearson")
pearson_string <- paste("Pearson correlation: ",round(pearson,3), sep="")
spearman <- cor(predictions_df$actual, predictions_df$predicted, method="spearman")
spearman_string <- paste("Spearman correlation: ",round(spearman,3), sep="")

# Calculate TPR and FPR using the following criteria:
#	no effect on splicing: -5 <= delta_psi <= 5
#	effect on splicing: delta_psi < -5 OR delta_psi > 5
predictions_df$actual_state <- ifelse(predictions_df$actual <= 5 & predictions_df$actual >= -5, 0, 1)
predictions_df$predicted_state <- ifelse(predictions_df$predicted <= 5 & predictions_df$predicted >= -5, 0, 1)

# Count number of TP, TN, FP, FN:
TP <- sum(predictions_df$predicted_state == 1 & predictions_df$actual_state == 1)
TN <- sum(predictions_df$predicted_state == 0 & predictions_df$actual_state == 0)
FP <- sum(predictions_df$predicted_state == 1 & predictions_df$actual_state == 0)
FN <- sum(predictions_df$predicted_state == 0 & predictions_df$actual_state == 1)

# Calculate TPR, FPR
TPR <- TP/(TP+FN)
FPR <- FP/(FP+TN)
TPR_string <- paste("True Positive Rate: ",round(TPR,3), sep="")
FPR_string <- paste("False Positive Rate: ",round(FPR,3), sep="")

# Plot correlation between actual values and predicted values using 2-dimensional bins
base_plot <- ggplot(predictions_df, aes(x=actual, y=predicted)) + geom_bin2d(bins=100) + theme_classic() + xlim(-100,100) + ylim(-100,100) + scale_fill_gradient(low='grey', high='red') + geom_vline(xintercept=c(-5,5), linetype="dotted") + geom_hline(yintercept=c(-5,5), linetype="dotted")
final_plot <- base_plot + xlab("Experimental HepG2 delta PSI") + ylab("Predicted HepG2 delta PSI") + theme(legend.title=element_blank()) + annotate("text", x=60, y=85, label=pearson_string) + annotate("text", x=60, y=77, label=spearman_string) + annotate("text", x=60, y=69, label=TPR_string) + annotate("text", x=60, y=61, label=FPR_string)
png(filename=output_fp, height=1200, width=1200, res=180)
final_plot
dev.off()