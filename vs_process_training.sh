#!/bin/bash

# Process input arguments: (1) training set of variants, (2) path to a tmp directory to store intermediate results
training_file="$1"
tmp="$2"

# Set up working directories
data="/data/rwang/data"
vexseq="$data/vexseq"
gencode="$data/gencode"

# Determine gene orientation of each test exon using GENCODE v19 annotations
tail -n +2 "$training_file" | cut -f2,6,7 | sort -k1,1 -k2,2n | uniq | bedtools intersect -a "$gencode/gencode.v19.exon.bed" -b - > "$tmp/vs_training_exon_coordinates.tsv"

# Setup new header for training file
echo -e "ID\tchromosome\thg19_variant_position\treference\tvariant\texon_start\texon_end\tHepG2_delta_logit_psi\tgene_orientation" > "$tmp/vs_training_set.tsv"

# Calculate delta logit PSI for each variant
tail -n +2 "$training_file" | awk '{
        if(FNR == NR) {
                exon_id = $1_$2_$3;
                orientation[exon_id] = $4;
        }
        else {
                ref_psi = $9/100;
                variant_psi = ($8+$9)/100;

                # Set the reference PSI to 1e-10 if it is 0.0.
                if(ref_psi == 0) {
                        ref_psi = 1e-10;
                }

                # Set the variant PSI to 1e-10 if it is not positive
                if(variant_psi <= 0) {
                        variant_psi = 1e-10;
                }
                delta_logit_psi = log(variant_psi/(1-variant_psi)) - log(ref_psi/(1-ref_psi));
                printf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",$1,$2,$3,$4,$5,$6,$7,delta_logit_psi,orientation[$2_$6_$7])
        }
}' "$tmp/vs_training_exon_coordinates.tsv" - | sort -k2,2 -k3,3n >> "$tmp/vs_training_set.tsv"

rm "$tmp/vs_training_exon_coordinates.tsv"