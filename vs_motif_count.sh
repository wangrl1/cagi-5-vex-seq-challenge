#!/bin/bash

score_frame()
{
        sequence="$1"
        list="$2"
        motif_count=0

        while read motif; do
                tmp_count=$(echo "$sequence" | tr 'T' 'U' | sed 's/$motif/./g' | tr -dc . | wc -c)
                motif_count=$(bc -l <<< "$motif_count+$tmp_count")
        done < "$list"

        echo "$motif_count"
}

export -f score_frame

score_motif()
{
        prefix=$(basename "$1")

        while read line; do
                chr=$(echo "$line" | cut -f2)
                pos=$(echo "$line" | cut -f3)
                ref=$(echo "$line" | cut -f4)
                alt=$(echo "$line" | cut -f5)
                strand=$(echo "$line" | cut -f9)

                lower_bound=$(bc -l <<< "$pos-14")
                upper_bound=$(bc -l <<< "$pos+14")
                sequence=$(tail -n +2 "$hg19/$chr.fa" | cut -c"$lower_bound"-"$upper_bound" | tr "[a-z]" "[A-Z]")

                printf "%s\t%s\t%s\t%s" "$chr" "$pos" "$ref" "$alt" >> "$tmp/$prefix.motif"

                while read group; do
                        ls "$data/$group" | for file in *_cluster_*.list; do
                                window_size=$(awk '{print length($0)}' "$data/$group/$file" | sort | uniq | tail -n 1)
                                lower_bound=$(bc -l <<< "16-$window_size")
                                upper_bound=$(bc -l <<< "14+$window_size")
                                refseq=$(echo "$sequence" | cut -c"$lower_bound"-"$upper_bound")

                                if [ "$strand" == "-" ]; then
                                        refseq=$(echo "$refseq" | sed 'y/ATCG/TAGC/;s/^.*$/X&X/;:x;s/\(X.\)\(.*\)\(.X\)/\3\2\1/;tx;s/X//g')
                                fi

                                count_ref=$(score_frame "$refseq" "$data/$group/$file")

                                # variants
                                len_ref=${#ref}
                                len_alt=${#alt}
                                seq_size=$(bc -l <<< "2*$window_size-1")

                                if [ "$len_ref" -gt "$len_alt" ]; then
                                        len_diff=$(bc -l <<< "$len_ref-$len_alt")
                                        upper_bound=$(bc -l <<< "14+$window_size+$len_diff")
                                fi  

                                refseq=$(echo "$sequence" | cut -c"$lower_bound"-"$upper_bound")

                                idx_left=$(bc -l <<< "$window_size-1")
                                alt_left=$(echo "$refseq" | cut -c1-"$idx_left")
                                idx_right=$(bc -l <<< "$idx_left+$len_ref+1")
                                alt_right=$(echo "$refseq" | cut -c"$idx_right"-)
                                altseq="$alt_left""$alt""$alt_right"

                                if [ "$len_ref" -lt "$len_alt" ]; then
                                        altseq=$(echo "$altseq" | cut -c1-"$seq_size")
                                fi

                                if [ "$strand" == "-" ]; then
                                        altseq=$(echo "$altseq" | sed 'y/ATCG/TAGC/;s/^.*$/X&X/;:x;s/\(X.\)\(.*\)\(.X\)/\3\2\1/;tx;s/X//g')
                                fi

                                count_alt=$(score_frame "$altseq" "$data/$group/$file")
                                count_diff=$(bc -l <<< "$count_alt-$count_ref")
                                printf "\t%s" "$count_diff" >> "$tmp/$prefix.motif"
                        done
                done < "$data/vexseq/vs_motif.list"
                echo "" >> "$tmp/$prefix.motif"

        done < "$1"
}

export data
export tmp
export hg19
export -f score_motif

data="/data/rwang/data"
hg19="$data/hg19"
tmp="/data/rwang/tmp"
threads=$(nproc)
input_file="$1"
src_dir=$(dirname "$0")

num_lines=$(tail -n +2 "$input_file" | wc -l)
lines_per_file=$(awk 'BEGIN {printf("%.0f",('"$num_lines"'+'"$threads"'-1)/'"$threads"')}')
tail -n +2 "$input_file" | split --lines="$lines_per_file" - "$tmp/vs_input."

find "$tmp" -name "vs_input.*" -print0 | parallel -0 -n 1 -P "$threads" -I '{}' score_motif '{}'

find "$tmp" -name "vs_input.*.motif" | while read file_motif; do
        cat "$file_motif" >> "$tmp/vs_input.motif.unsorted"
done

printf "%s\t%s\t%s\t%s" "chromosome" "hg19_variant_position" "reference" "variant" >> "$tmp/vs_training_set.motif"

while read group; do
        ls "$data/$group/profile" | while read profile; do
                cluster_name=$(echo "$profile" | cut -d '.' -f1 | sed 's/_cluster//')
                printf "\t%s" "$cluster_name" >> "$tmp/vs_training_set.motif"
        done
done < "$data/vexseq/vs_motif.list"

echo "" >> "$tmp/vs_training_set.motif"
sort -k1,1 -k2,2n "$tmp/vs_input.motif.unsorted" >> "$tmp/vs_training_set.motif"

rm $tmp/vs_input.*