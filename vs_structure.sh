#!/bin/bash

# Process input arguments: (1) input file of variants, (2) path to a tmp directory to store intermediate results
input_file="$1"
tmp="$2"
prefix_out=$(basename "$input_file" | cut -d '.' -f1)

# Set up header for output file
echo -e "chromosome\thg19_variant_position\treference\tvariant\tnearest_ss_dist\texon_length\tisTv\tindel_size\t" > "$tmp/$prefix_out.structure"

tail -n +2 "$input_file" | awk 'BEGIN {
                # array mapping nucleotides to respective transition mutations
                transition["A"] = "G";
                transition["G"] = "A";
                transition["C"] = "T";
                transition["T"] = "C";
        }; {
                pos = $3;
                start = $6;
                stop = $7;

                # one hot encoding of variant position with respect to test exon
                upstream_3ss = 0;
                exon = 0;
                downstream_5ss = 0;

                # default values initialized to 0
                isTv = 0;
                exon_length = stop - start;

                # calculate indel size
                indel_size = length($5) - length($4);
                if(indel_size == 0) {
                        if(transition[$4] != $5) {
                                isTv = 1;
                        }
                }

                # variant is located within exon
                if(pos >= start && pos <= stop) {
                        exon = 1;
                        dist1 = pos - start;
                        dist2 = stop - pos;
                        if(dist1 <= dist2) {
                                ss_dist = dist1;
                        }
                        else {
                                ss_dist = dist2;
                        }
                }
                else {
                        if(pos > stop){
                                ss_dist = pos - stop;
                                if($9 == "+") {
                                        downstream_5ss = 1;
                                }      
                                else {
                                        upstream_3ss = 1;
                                }
                        }
                        else{
                                ss_dist = start - pos;
                                if($9 == "+") {
                                        upstream_3ss = 1;
                                }      
                                else {
                                        downstream_5ss = 1;
                                }
                        }
                }
                printf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",$2,$3,$4,$5,ss_dist,exon_length,isTv,indel_size,upstream_3ss,exon,downstream_5ss);
}' | sort -k1,1 -k2,2n >> "$tmp/$prefix_out.structure"