#!/bin/bash

annotate_variants()
{
        # This is a function for annotating variants using the following features:
        #       1. CADD features (conservation, GC content)
        #       2. exon length, distance to nearest splice site, variant properties
        #       3. effects of variants on RNA secondary structure formation
        #       4. disruption of 5'-splice site (MaxEntScan::5ss)
        #       5. disruption of 3'-splice site (MaxEntScan::3ss)
        #       6. disruption of branch point sequence (SVM-BP)
        #       7. disruption of splicing regulatory elements using maxixmum entropy modeling

        mode=$(basename "$1" | cut -d '.' -f1 | rev | cut -d '_' -f1 | rev)

        # Process input file by including logit transformed HepG2 delta PSI values and strand
        # orientation of each test exon
        if [ "$mode" == "training" ]; then
                bash "$src/vs_process_training.sh" "$1" "$outdir/tmp"
        else
                bash "$src/vs_process_test.sh" "$1" "$outdir/tmp"
        fi

        # Annotate variants using features from CADD version 1.3
        bash "$src/vs_cadd.sh" "$outdir/tmp/vs_""$mode""_set.tsv" "$vexseq/vs_cadd-1.3_feature.list" "$outdir/tmp"

        # Annotate variants by transcript structure features (exon length, distance to nearest splice splice site)
        bash "$src/vs_structure.sh" "$outdir/tmp/vs_""$mode""_set.tsv" "$outdir/tmp"

        # Annotate variants based on effects on RNA secondary structure
        bash "$src/vs_rnafold.sh" "$outdir/tmp/vs_""$mode""_set.tsv"  "$outdir/tmp"

        # Annotate variants using MaxEntScan::5ss
        bash "$src/vs_5ss.sh" "$outdir/tmp/vs_""$mode""_set.tsv" "$outdir/tmp"

        # Annotate variants using MaxEntScan::3ss
        bash "$src/vs_3ss.sh" "$outdir/tmp/vs_""$mode""_set.tsv" "$outdir/tmp"

        # Annotate variants using SVMBP
        bash "$src/vs_svmbp.sh" "$outdir/tmp/vs_""$mode""_set.tsv" "$outdir/tmp"

        # Annotate variants using maximum entropy modeling of splicing regulatory motifs
        bash "$src/vs_motif.sh" "$outdir/tmp/vs_""$mode""_set.tsv" "$outdir/tmp"

        # Combine annotations into one large table
        cat "$outdir/tmp/vs_""$mode""_set.tsv" > "$outdir/tmp/vs_""$mode""_set.table"
        for group in "cadd" "structure" "5ss" "3ss" "svmbp" "motif"; do
                cut -f5- "$outdir/tmp/vs_""$mode""_set.$group" | paste "$outdir/tmp/vs_""$mode""_set.table" - > "$outdir/tmp/vs_""$mode""_set.table.tmp"
                rm "$outdir/tmp/vs_""$mode""_set.table"
                mv "$outdir/tmp/vs_""$mode""_set.table.tmp" "$outdir/tmp/vs_""$mode""_set.table"
        done
        cut -f1,8,10- "$outdir/tmp/vs_""$mode""_set.table" > "$outdir/vs_""$mode""_set.table"

}

export outdir
export src
export -f annotate_variants

# Process input arguments: (1) training file, (2) test file
training_file="$1"
test_file="$2"

# Set up working directories
vexseq="/data/rwang/data/vexseq"
src="/data/rwang/vexseq/src"

# Construct output directory
runtime=$(date +"%Y%m%d%H%M%S%N")
outdir="/data/rwang/test/vs-$runtime"
mkdir -p "$outdir"
mkdir -p "$outdir/tmp"

# Annotate variants in training and test files:
annotate_variants "$training_file"
annotate_variants "$test_file"

# Train random forest model with the following input parameters:
#       (1) training set of annotated variants
#       (2) test set of annotated variants
#       (3) path to file storing model predictions
#       (4) path to file storing important features

Rscript "$src/random_forest.R" "$outdir/vs_training_set.table" "$outdir/vs_test_set.table" "$outdir/vs_test_set.table.predictions" "$outdir/vs_model.table.varImp"

sed -i 's/"//g' "$outdir/vs_test_set.table.predictions"
sed -i 's/"//g' "$outdir/vs_model.table.varImp"

# Construct a file comparing experimentally measured delta PSI values to model predicted delta PSI values
tail -n +2 "$test_file" | awk '{
        if(FNR==NR) {
                delta_psi[$1]=$8;
                ref_psi[$1]=$9;
        }
        else {
                x=ref_psi[$1];
                experimental_delta_psi=delta_psi[$1];
                predicted_delta_psi=(x*(100-x)*(exp($2)-1))/(100-x+x*exp($2));
                printf("%s\t%s\t%s\n",$1,experimental_delta_psi,predicted_delta_psi)
        }
}' - "$outdir/vs_test_set.table.predictions" >> "$outdir/vs_test_set.table.predictions.tmp"

rm "$outdir/vs_test_set.table.predictions"
mv "$outdir/vs_test_set.table.predictions.tmp" "$outdir/vs_test_set.table.predictions"

# Construct a plot depicting the correlation between model predicted delta PSI values and experimentally measured delta PSI values
Rscript "$src/correlation_plots.R" "$outdir/vs_test_set.table.predictions" "$outdir/vs_test_set.table.predictions.png"

# Identify the top 20 important features used by the model during training
sort -k2,2rg "$outdir/vs_model.table.varImp" | head -n 20 > "$outdir/vs_model.table.varImp.tmp"
rm "$outdir/vs_model.table.varImp"
mv "$outdir/vs_model.table.varImp.tmp" "$outdir/vs_model.table.varImp"

# Construct a barplot comparing the relative importances of different features used by the model
Rscript "$src/feature_importance_plots.R" "$outdir/vs_model.table.varImp" "$outdir/vs_model.table.varImp.png"