#!/bin/bash

score_frame()
{
        sequence="$1"
        profile="$2"
        prefix="$3"
        window_size="$4"

        for (( i=1; i<="$window_size"; i++ )); do
                j=$(bc -l <<< "$i+$window_size-1")
                echo "$sequence" | cut -c"$i"-"$j" >> "$tmp/$prefix.frame"
        done

        max_prob=$(awk '{if(FNR==NR){prob[$1]=$2;}else{gsub("T","U",$1);printf("%s\n",prob[$1]);}}' "$profile" "$tmp/$prefix.frame" | sort -rg | head -n 1)
        rm "$tmp/$prefix.frame"
        echo "$max_prob"
}

export tmp
export -f score_frame

score_motif()
{
        # This function calculates the change in putative splicing regulatory element
        # strength due to a mutation using maximum entropy modeling

        prefix=$(basename "$1")

        while read line; do
                variant_id=$(echo "$line" | cut -f1)
                chr=$(echo "$line" | cut -f2)
                pos=$(echo "$line" | cut -f3)
                ref=$(echo "$line" | cut -f4)
                alt=$(echo "$line" | cut -f5)
                strand=$(echo "$line" | cut -f9)

                lower_bound=$(bc -l <<< "$pos-14")
                upper_bound=$(bc -l <<< "$pos+14")

                # Extract reference sequence specified by lower and upper bounds
                sequence=$(tail -n +2 "$hg19/$chr.fa" | cut -c"$lower_bound"-"$upper_bound" | tr "[a-z]" "[A-Z]")

                printf "%s\t%s\t%s\t%s" "$chr" "$pos" "$ref" "$alt" >> "$tmp/$prefix.motif"

                # Iterate through databases of splicing regulatory motifs validated through
                # wet experiments
                while read group; do
                        ls "$data/$group/profile" | while read profile; do
                                # Iterate through groups of motif sequences clustered based on string edit distance
                                cluster_name=$(basename "$profile" | cut -d '.' -f1)
                                factor_string=$(grep -P "\t$cluster_name\t" "$data/vexseq/vs_motif_consensus.tsv" | cut -f4)

                                # Checks if cluster of motif sequences has an associated splicing factor
                                if [ "$factor_string" != "NA" ]; then
                                        # Extract sequence window relevant to family of motif sequences
                                        window_size=$(cut -f1 "$data/$group/profile/$profile" | awk 'END{print length($0)}')
                                        lower_bound=$(bc -l <<< "16-$window_size")
                                        upper_bound=$(bc -l <<< "14+$window_size")
                                        refseq=$(echo "$sequence" | cut -c"$lower_bound"-"$upper_bound")

                                        # Take reverse complement of sequence if orientation is -
                                        if [ "$strand" == "-" ]; then
                                                refseq=$(echo "$refseq" | sed 'y/ATCG/TAGC/;s/^.*$/X&X/;:x;s/\(X.\)\(.*\)\(.X\)/\3\2\1/;tx;s/X//g')
                                        fi

                                        #prob_ref=$(score_frame "$refseq" "$data/$group/profile/$profile" "$prefix" "$window_size")

                                        len_ref=${#ref}
                                        len_alt=${#alt}
                                        seq_size=$(bc -l <<< "2*$window_size-1")

                                        # Deletion
                                        if [ "$len_ref" -gt "$len_alt" ]; then
                                                len_diff=$(bc -l <<< "$len_ref-$len_alt")
                                                upper_bound=$(bc -l <<< "14+$window_size+$len_diff")
                                        fi

                                        # Construct mutated sequence
                                        refseq=$(echo "$sequence" | cut -c"$lower_bound"-"$upper_bound")
                                        idx_left=$(bc -l <<< "$window_size-1")
                                        alt_left=$(echo "$refseq" | cut -c1-"$idx_left")
                                        idx_right=$(bc -l <<< "$idx_left+$len_ref+1")
                                        alt_right=$(echo "$refseq" | cut -c"$idx_right"-)
                                        altseq="$alt_left""$alt""$alt_right"

                                        # Insertion
                                        if [ "$len_ref" -lt "$len_alt" ]; then
                                                altseq=$(echo "$altseq" | cut -c1-"$seq_size")
                                        fi

                                        # Take reverse complement of sequence if orientation is -
                                        if [ "$strand" == "-" ]; then
                                                altseq=$(echo "$altseq" | sed 'y/ATCG/TAGC/;s/^.*$/X&X/;:x;s/\(X.\)\(.*\)\(.X\)/\3\2\1/;tx;s/X//g')
                                        fi

                                        # Compute likelihood score
                                        likelihood_score=$(python "$src_dir/vs_motif.py" "$refseq" "$altseq" "$data/$group/profile/$profile" "$window_size" "$factor_string" "$chr" "$pos" "$strand" "$variant_id" "$tmp/$prefix_out")
                                        printf "\t%s" "$likelihood_score" >> "$tmp/$prefix.motif"
                                fi
                        done
                done < "$data/vexseq/vs_motif.list"
                echo "" >> "$tmp/$prefix.motif"

        done < "$1"
}

export data
export tmp
export src_dir
export prefix_out
export hg19
export -f score_motif

# Process input arguments: (1) input file of variants, (2) path to a tmp directory to store intermediate results
input_file="$1"
tmp="$2"

# Set up working directories
data="/data/rwang/data"
hg19="$data/hg19"
src_dir=$(dirname "$0")
prefix_out=$(basename "$input_file" | cut -d '.' -f1)

threads=$(nproc)
num_lines=$(tail -n +2 "$input_file" | wc -l)
lines_per_file=$(awk 'BEGIN {printf("%.0f",('"$num_lines"'+'"$threads"'-1)/'"$threads"')}')
tail -n +2 "$input_file" | split --lines="$lines_per_file" - "$tmp/vs_input."

# Split input file of variants for parallel processing
find "$tmp" -name "vs_input.*" -print0 | parallel -0 -n 1 -P "$threads" -I '{}' score_motif '{}'

# Combine annotations into one file
find "$tmp" -name "vs_input.*.motif" | while read file_motif; do
        cat "$file_motif" >> "$tmp/vs_input.motif.unsorted"
done

# Set up header for output file
printf "%s\t%s\t%s\t%s" "chromosome" "hg19_variant_position" "reference" "variant" > "$tmp/$prefix_out.motif"

while read group; do
        ls "$data/$group/profile" | while read profile; do
                cluster_name=$(echo "$profile" | cut -d '.' -f1)
                factor_string=$(grep -P "\t$cluster_name\t" "$data/vexseq/vs_motif_consensus.tsv" | cut -f4)
                if [ "$factor_string" != "NA" ]; then
                        final_name=$(echo "$cluster_name" | sed 's/_cluster//')
                        printf "\t%s" "$final_name" >> "$tmp/$prefix_out.motif"
                fi
        done
done < "$data/vexseq/vs_motif.list"

echo "" >> "$tmp/$prefix_out.motif"
sort -k1,1 -k2,2n "$tmp/vs_input.motif.unsorted" >> "$tmp/$prefix_out.motif"

rm $tmp/vs_input.*